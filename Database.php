<?php

namespace FpDbTest;

use Exception;
use mysqli;

class Database implements DatabaseInterface
{
    const OPERATION_LENGTH                = 1;
    const OPERATION_WITH_SPECIFIER_LENGTH = 2;

    private mysqli $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function buildQuery(string $query, array $args = []): string
    {
        $specialBlock = $this->getBordersSpecialBlock($query);

        foreach ($args as $arg) {
            $index = strpos($query, '?');
            $currentOperation = mb_substr($query, $index, self::OPERATION_WITH_SPECIFIER_LENGTH);
            $query = $this->processSpecialBlock($query, $arg, $index, $specialBlock, $currentOperation);

            if ($currentOperation === '?d') {
                $query = $this->prepareInt($query, $arg, $index);
            } elseif ($currentOperation === '?f') {
                $query = $this->prepareDouble($query, $arg, $index);
            } elseif ($currentOperation === '?a') {
                $query = $this->prepareArray($query, $arg, $index);
            } elseif ($currentOperation === '?#') {
                $query = $this->prepareArrayIds($query, $arg, $index);
            } elseif ($currentOperation == '? ') {
                $query = $this->prepareUniversalType($query, $arg, $index);
            } elseif ($currentOperation == '') {
                continue;
            } else {
                throw new Exception();
            }

            $specialBlock = $this->getBordersSpecialBlock($query);;
        }

     return $query;
    }

    protected function prepareInt(string $query, $arg, int $index): string
    {
        return substr_replace($query, (int)$arg, $index, self::OPERATION_WITH_SPECIFIER_LENGTH);
    }

    protected function prepareDouble(string $query, $arg, int $index): string
    {
        return substr_replace($query, (double)$arg, $index, self::OPERATION_WITH_SPECIFIER_LENGTH);
    }

    protected function prepareArray(string $query, $arg, int $index): string
    {
        $stringFromArray = '';

        foreach ($arg as $key => $item) {
            $item = is_string($item) ? "'$item'" : $item;
            $item = $item ?? 'NULL';

            $keyValue = is_string($key) ? "`$key` = " : '';
            $stringFromArray .= $keyValue . $item . ', ';
        }

        $stringFromArray = $this->trimSymbolsFromTheEnd($stringFromArray, -2);
        return substr_replace($query, $stringFromArray, $index, self::OPERATION_WITH_SPECIFIER_LENGTH);
    }

    protected function prepareArrayIds(string $query, $arg, int $index): string
    {
        $stringIds = '';

        if (is_array($arg)) {
            foreach ($arg as $item) {
                $stringIds .= "`$item`, ";
            }

            $stringIds = $this->trimSymbolsFromTheEnd($stringIds, -2);
        } else {
            $stringIds = "`$arg`";
        }

        return substr_replace($query, $stringIds, $index, self::OPERATION_WITH_SPECIFIER_LENGTH);
    }

    protected function prepareUniversalType(string $query, $arg, int $index): string
    {
        $value = is_string($arg) ? "'$arg'" : $arg;
        return substr_replace($query, $value, $index, self::OPERATION_LENGTH);
    }

    protected function processSpecialBlock(string $query, $arg, int &$index, array $specialBlock, string &$currentOperation): string
    {
        $isSpecialBlock = $index >= $specialBlock['start'] && $index <= $specialBlock['end'];

        if ($isSpecialBlock && !$arg) {
            $query = substr_replace($query, '', $specialBlock['start'], $specialBlock['end'] - $specialBlock['start'] + 1);
            $currentOperation = '';
        } elseif ($isSpecialBlock) {
            $query = substr_replace($query, '', $specialBlock['start'], 1);
            $query = substr_replace($query, '', $specialBlock['end'] - 1, 1);
            $index--;
        }

        return $query;
    }
    protected function getBordersSpecialBlock(string $query): array
    {
        return ['start' => strpos($query, '{'), 'end' => strpos($query, '}')];
    }
    protected function trimSymbolsFromTheEnd($str, $length)
    {
        return substr($str, 0, $length);
    }

    public function skip(): bool
    {
        return false;
    }
}
